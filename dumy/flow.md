## Project flow

* Created a project with the name of dumy
* Inside project created an app with the name of assessment
* Inside project created a utils.py where I am accessing the mongodb database

* Inside assessment app urls.py contains 2 urls
    1. <http://127.0.0.1:8000/apiinfo>
    2. <http://127.0.0.1:8000/table>

* I have added Cross-Origin Resource Sharing (CORS) headers app to settings and set CORS_ALLOW_ALL_ORIGINS = True
* In view I made function based view which handels GET api request and after sorting it on the basis newly created transaction it returns the json response after converting bson format to json format.
* And another function named as fetch_api, which is used to fetch the data using api.
* After that I am checking the request if it is POST request then storing the filter condition and filter value in two variables, then I am storing the filtered data
* After that I am creating paginator for both GET and POST requests.
* Finally I am rendering the html page by passing page_obj to html page.

`I have also created one more project which is hlcAssessment`

In this I am fecthicg the data from <http://127.0.0.1:8000/apiinfo> using ajax.

Here I also did pagination, filtering functionalities.
