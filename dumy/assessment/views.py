from rest_framework.decorators import api_view
from rest_framework.response import Response
from utils import get_db
from bson import json_util
from django.shortcuts import render
from django.core.paginator import Paginator
import requests
import json


@api_view(['GET'])
def api_info(request):
    # loading database from utils.py
    db = get_db()
    collection = db["hlcCollection"]
    data = collection.find().sort('created_on', -1)

    # converting bson to json
    json_obj = json.loads(json_util.dumps(data))
    return Response(json_obj)


def fetch_api(request):
    # fetching the data using api
    json_data = requests.get('http://127.0.0.1:8000/apiinfo').json()
    
    # Converting field (request_payload) from string to json
    for document in json_data:
        document['request_payload'] = json.loads(document['request_payload'])


    if request.method == 'POST':
        if request.POST.get('transaction_id'):
            filter_condition = "transaction_id"
            value = request.POST.get('transaction_id')
        elif request.POST.get('api_type'):
            filter_condition = "api_type"
            value = request.POST.get('api_type')
        elif request.POST.get('customer_id'):
            filter_condition = "customer_id"
            value = request.POST.get('customer_id')
        elif request.POST.get('roster_sap_id'):
            filter_condition = "roster_sap_id"
            value = request.POST.get('roster_sap_id')

        # Filtering the sorted data based on given input value
        if filter_condition == "customer_id":
            json_data = filter(
                lambda x: x["request_payload"]["ZapiProdorderStartTrigger"]["CustomerReference"] == value, json_data)
        else:
            json_data = filter(
                lambda x: x[filter_condition] == value, json_data)

    # Pagination
    paginator_obj = Paginator(list(json_data), 10)
    page = request.GET.get('page')
    page_obj = paginator_obj.get_page(page)

    context = {
        'page_obj': page_obj
    }

    return render(request, 'assessment/index.html', context=context)
