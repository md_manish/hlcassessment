from django.urls import path
from . import views

app_name = 'assessment'

urlpatterns = [
    path('apiinfo', views.api_info),
    path('table', views.fetch_api)
]
