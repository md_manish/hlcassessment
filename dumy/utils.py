from pymongo import MongoClient

def get_db():
    myclient = MongoClient("mongodb://localhost:27017/")
    db = myclient["hlcdb"]
    return db
