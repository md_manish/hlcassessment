from django.shortcuts import render


def fetch_api(request):
    return render(request, 'apidata/index.html')
