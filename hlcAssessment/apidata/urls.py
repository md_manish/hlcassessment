from django.urls import path
from . import views

app_name = 'apidata'

urlpatterns = [
    path('table', views.fetch_api)
]
